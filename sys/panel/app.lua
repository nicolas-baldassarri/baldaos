local options = {"Programmi", "Installa",  "Crediti", "Riavvia", "Aggiorna OS", "Formatta"}
local links = {"/sys/panel/programs/app", "/sys/install", "/sys/credits", "reboot", "/sys/update", "/sys/format"}
local sel = 1
local w,h = term.getSize()
term.setTextColor(colors.black)
function centertext(text, y)
  term.setCursorPos((w/2)-string.len(text)/2, y)
  term.write(text)
end
function drawMenu()
  term.clear()
  centertext("BALDA OS", 2)
  centertext("Homepage", 3)
  for i=1,#options do
    if i == sel then
      centertext("["..options[i].."]", (h/2)+i)
    else
      centertext(options[i], (h/2)+i)
    end
  end
end
term.setBackgroundColor(colors.black)
term.setTextColor(colors.white)
while true do
  drawMenu()
  event, key = os.pullEvent("key")
  if key == keys.up then
    sel = sel-1
  end
  if key == keys.down then
    sel = sel+1
  end
  if key == keys.enter then
    shell.run(links[sel])
  end
  if sel > #options then
    sel = #options
  end
  if sel < 0 then
    sel = 0
  end
end