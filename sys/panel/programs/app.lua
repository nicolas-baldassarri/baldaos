local sel = 1
local w,h = term.getSize()
 
local programs = {}
local links = {}
 
local files = fs.list("/apps/")
for i=1,#files do
    links[i] = "/apps/"..files[i].."/app.lua"                         
    programs[i] = files[i]
end
 
table.insert(programs, "Indietro")
table.insert(links, "sys/panel/app")
 
term.setTextColor(colors.black)
function centertext(text, y)
  term.setCursorPos((w/2)-string.len(text)/2, y)
  term.write(text)
end
function drawMenu()
  term.clear()
  centertext("BALDA OS", 2)
  centertext("Lista Programmi", 3)
  for i=1,#programs do
    if i == sel then
      centertext("["..programs[i].."]", (h/2)+i)
    else
      centertext(programs[i], (h/2)+i)
    end
  end
end
term.setBackgroundColor(colors.black)
term.setTextColor(colors.white)
while true do
  drawMenu()
  event, key = os.pullEvent("key")
  if key == keys.up then
    sel = sel-1
  end
  if key == keys.down then
    sel = sel+1
  end
  if key == keys.enter then
    shell.run(links[sel])
  end
  if sel > #programs then
    sel = #programs
  end
  if sel < 0 then
    sel = 0
  end
end