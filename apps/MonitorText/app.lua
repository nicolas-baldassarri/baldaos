term.clear()
term.setCursorPos(1,1)
term.write("Posizione monitor: ")
local position = read()
term.write("Testo: ")
local text = read()

local mon = peripheral.wrap(position)
mon.setTextScale(2)
mon.clear()

x,y = mon.getSize()
x1,y1 = mon.getCursorPos()
mon.setCursorPos((math.floor(x/2) - (math.floor(#text/2))), y1)
mon.write(text)
